function calculator (a, b, action) {

    a = parseFloat( prompt('Write first number: ', a));
    b = parseFloat( prompt('Write second number ', b));

    action = prompt('Enter operator + or - or / or *', action);

    let res = 0;
    if(isNaN(a) || isNaN(b)) {
        calculator(a, b , action)
    } else {

        if(action == '+') {
            res = a + b;
        } else if(action == '-' ) {
            res = a - b;
        } else if(action == '/') {
            res = a / b;
        } else if (action == '*') {
            res = a * b;
        }
        console.log(res)
    }
}

calculator()